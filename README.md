# CRUD Usuarios

Proyecto API en NodeJS que contiene un CRUD para modificar los datos de un usuarios.


## Instalación

Antes de instalar asegurese tener instalado Mongo DB en su equipo o disponer de una base de datos para su conexción.

Debes clonar este proyecto ejecutando:

`git clone git@gitlab.com:millan2993/typescript-nodejs.git`

Instalar sus dependencias con: 

`npm install`


### Ejecución

Antes que nada se debe configurar la conexión a la base de datos, esto se configura en la función connectDatabase() del archivo src/app.ts.

Debido a que este proyecto está usando TypeScript es importante transpilar este código a Javascript. Esto se logra ejecutando:

`npm run build`

Para colocar en ejecución este proyecto en un entorno en producción se debe ejecutar:

`npm run prod`

Si es un entorno de desarrollo se ejecuta: `npm run dev`

Si deseamos que se actualice automáticamente al haber cambios en los archivos se debe ejecutar: 
`npm start`. 
Asegurese de tener instalado nodemon. Para instalarlo ejecute: 
`npm install nodemon --dev-save`  

### Herramientas
###### NodeJs
###### Typescript
###### Express
###### Mongo DB



import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as mongoose from 'mongoose';
import * as cors from 'cors';

import UserController from './modules/users/UserController';

class App {

    public app: express.Application;

    constructor() {
        this.app = express();

        this.config();

        this.connectDatabase();
        this.initializeControllers();
    }


    private config(): void {
        this.app.use(cors());

        // support application/json type post data
        this.app.use(bodyParser.json());

        // support application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({extended: true}));

    }


    private connectDatabase(): void {
        mongoose.connect('mongodb://localhost/users', {useNewUrlParser: true})
            .then(() => console.log('connection successful'))
            .catch((err) => console.error(err));
    }


    private initializeControllers() {
        this.app.use(new UserController().router);
    }

}


export default new App().app;

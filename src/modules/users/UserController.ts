import * as express from 'express';
import userModel from "./user.model";

class UserController {

    private path = '/users';
    public router = express.Router();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.get(this.path, this.getUsers);
        this.router.post(this.path, this.createUser);
        this.router.get(this.path + '/:id', this.getUser);
        this.router.put(this.path + '/:id', this.editUser);
        this.router.delete(this.path + '/:id', this.deleteUser);
        this.router.patch(this.path + '/inactive/:id', this.inactiveUser);
        this.router.patch(this.path + '/active/:id', this.activeUser);
    }


    getUsers = (request: express.Request, response: express.Response) => {
        userModel.find()
            .then(users => {
                response.send(users);
            });
    };


    createUser = (request: express.Request, response: express.Response) => {
        const params = request.body;

        const user = new userModel(params);
        user.save()
            .then((userSaved) => {
                response.send(userSaved);
            })
            .catch(err => {
                console.error(err);
            });
    };


    getUser = (request: express.Request, response: express.Response) => {
        const id = request.params.id;

        userModel.findById(id)
            .then(user => {
                response.send(user);
            });
    };


    editUser = (request: express.Request, response: express.Response) => {
        const id = request.params.id;
        const params = request.body;

        userModel.findByIdAndUpdate(id, params, { new: true })
            .then(user => {
                response.send(user);
            });
    };


    deleteUser = (request: express.Request, response: express.Response) => {
        const id = request.params.id;

        userModel.findByIdAndDelete(id)
            .then(deleted => {
                if (deleted) {
                    response.send(true);
                } else {
                    response.send(false);
                }
            })
    };


    inactiveUser = (request: express.Request, response: express.Response) => {
        response.send('inactive user');
    };

    activeUser = (request: express.Request, response: express.Response) => {
        response.send('active user');
    };

}


export default UserController;

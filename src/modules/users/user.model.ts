import * as mongoose from 'mongoose';
import {IUser} from "./IUser";

const userSchema = new mongoose.Schema({
    name: String,
    email: String
});

const userModel = mongoose.model<IUser & mongoose.document>('User', userSchema);

export default userModel;